package main

import (
	"fmt"
	"github.com/caarlos0/env/v11"
	"os"
)

var Conf Config
var meetingRequestChannel chan MeetingCreationRequest
var chatResponseChannel chan string
var terminateChannel chan int

func main() {
	Conf = Config{}
	err := env.Parse(&Conf)
	if err != nil {
		Close(err, 1)
	}

	meetingRequestChannel = make(chan MeetingCreationRequest)
	chatResponseChannel = make(chan string)

	go matrix()
	for {
		select {
		case tReq := <-meetingRequestChannel:
			tMeeting, err := CreateBbbMeeting(tReq)
			if err != nil {
				panic(err)
			}
			tLinks := CreateMeetingLinks(tMeeting.MeetingID, tReq.Moderator, tReq.Members)

			tMesg := CreateChatMessage(tMeeting, tLinks)

			chatResponseChannel <- tMesg

		}
	}

}

func Close(err error, exitCode int) {
	fmt.Println("err: ")
	fmt.Println(err)
	os.Exit(exitCode)
}

type MeetingCreationRequest struct {
	Members   []string
	Moderator string
}

func CreateChatMessage(resp BBBCreateMeetingResponseBody, links BBBMeetingLinks) string {
	var msg = fmt.Sprintf("<p><b>Successfully created meeting.</b></p>\n "+
		"<p>Meeting ID:%s</p> "+
		"<p>Dial Number:%s</p>",
		resp.MeetingID,
		resp.DialNumber,
	)

	msg = msg + fmt.Sprintf("<p><a href=\"%s\">Join as %s (Moderator)</a></p>\n", links.ModeratorLink.Url, links.ModeratorLink.Name)
	for _, key := range links.UserLinks {
		msg = msg + fmt.Sprintf("<p><a href=\"%s\">Join as %s</a></p>\n", key.Url, key.Name)
	}
	return msg
}
