# matrix-bigbluebutton-bot-go

This is a matrix bot that can create BigBlueButton rooms. It is written in go and uses [mautrix-go](https://github.com/mautrix/go/).

![structure](doc/structure.png "Title")

# Setup

## install dependencies

`go get`

## config

Configuration is taken from environment variables. Make sure to set them before running the program.

* MATRIX_HOMESERVER: (required) your matrix server
* MATRIX_USER: (required) matrix user for your bot
* MATRIX_PASSWORD: (required) password of your matrix user
* BBB_SERVER= (required) your BigBlueButton Server (make sure to include "/bigbluebutton/api/" at the end, e.g. https://example.com/bigbluebutton/api/)
* BBB_SECRET: (required) your BigBlueButton Secret (find this out by running `bbb-conf --secret` on your BigBlueButton server)
* BBB_MEETING_TITLE: (required) title of the BigBlueButton meeting that is created. Can only contain letters and numbers.
* PICKLE_KEY: (required) random string (used for encryption).
* DATABASE: filename for the runtime database. default: "runtime-data.db"
* MODERATOR_PW: moderator password for BBB meetings. default: "m0d3r4t0r"
* ATTENDEE_PW: user password for BBB meetings. default:"4tt3nd33" 

# Launch bot service

Run `go build`. This will create an executable named "matrix-bigbluebutton-bot-go".

As soon as the bot is online, it will output "Bot started!" in the console. 

# Usage

You first have to invite the bot to a room, from which you want to start a conference, just like a regular user. It will listen to room events.

If you now enter the command '!conf' the bot, will create a BigBlueButton Room. The relevant data (login URLs etc.) is then sent to the room, so others can easily join.

# Limitations

Please note that the bot based on golang is still in a proof-of-concept phase, so do not use it in production environments yet.

# Author

Martin Klampfer

An open source project by [fairkom.eu](https://fairkom.eu)
