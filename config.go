package main

type Config struct {
	MatrixHomeserver string `env:"MATRIX_HOMESERVER,required"`
	MatrixUser       string `env:"MATRIX_USER,required"`
	MatrixPassword   string `env:"MATRIX_PASSWORD,required"`
	BbbServer        string `env:"BBB_SERVER,required"`
	BbbSecret        string `env:"BBB_SECRET,required"`
	BbbMeetingTitle  string `env:"BBB_MEETING_TITLE,required"`
	PickleKey        string `env:"PICKLE_KEY,required""`
	Database         string `env:"DATABASE" envDefault:"runtime-data.db"`
	ModeratorPW      string `env:"MODERATOR_PW" envDefault:"m0d3r4t0r"`
	AttendeePW       string `env:"ATTENDEE_PW" envDefault:"4tt3nd33"`
}
