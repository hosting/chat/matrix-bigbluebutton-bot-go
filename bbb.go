package main

import (
	"crypto/sha1"
	"encoding/hex"
	"encoding/xml"
	"errors"
	"fmt"
	"github.com/google/uuid"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"
)

func CreateQueryString(method string, params []BBBUrlParam) string {
	queryString := ""
	for idx, key := range params {
		queryString = queryString + fmt.Sprintf("%s=%s", key.name, url.QueryEscape(key.value))
		if idx+1 < len(params) {
			queryString = queryString + fmt.Sprintf("&")
		}
	}

	p := fmt.Sprintf("%s%s%s", method, queryString, Conf.BbbSecret)
	h := sha1.New()
	h.Write([]byte(p))
	hash := hex.EncodeToString(h.Sum(nil))
	queryStringWithHash := fmt.Sprintf("%s&checksum=%s", queryString, hash)
	return queryStringWithHash
}

func CreateJoinUrl(meetingID string, userName string, isModerator bool) BBBJoinLink {
	tJoinParams := []BBBUrlParam{}
	tJoinParams = append(tJoinParams, BBBUrlParam{
		name:  "meetingID",
		value: meetingID,
	})
	tJoinParams = append(tJoinParams, BBBUrlParam{
		name:  "name",
		value: Conf.BbbMeetingTitle,
	})
	if isModerator {
		tJoinParams = append(tJoinParams, BBBUrlParam{
			name:  "password",
			value: Conf.ModeratorPW,
		})
	} else {
		tJoinParams = append(tJoinParams, BBBUrlParam{
			name:  "password",
			value: Conf.AttendeePW,
		})
	}
	tJoinParams = append(tJoinParams, BBBUrlParam{
		name:  "fullName",
		value: userName,
	})

	tQs := CreateQueryString("join", tJoinParams)
	bbbJoinUrl := Conf.BbbServer + "join?" + tQs

	tLink := BBBJoinLink{
		Name: userName,
		Url:  bbbJoinUrl,
	}
	return tLink
}

func CreateMeetingLinks(meetingID string, moderator string, users []string) BBBMeetingLinks {
	tLinks := BBBMeetingLinks{
		ModeratorLink: BBBJoinLink{},
		UserLinks:     nil,
	}

	tLinks.ModeratorLink = CreateJoinUrl(meetingID, moderator, true)

	for _, user := range users {
		tLink := CreateJoinUrl(meetingID, user, false)
		tLinks.UserLinks = append(tLinks.UserLinks, tLink)
	}
	return tLinks

}

func CreateCreateUrl(meetingID string) string {
	tParams := []BBBUrlParam{}
	tParams = append(tParams, BBBUrlParam{
		name:  "meetingID",
		value: meetingID,
	})
	tParams = append(tParams, BBBUrlParam{
		name:  "name",
		value: Conf.BbbMeetingTitle,
	})
	tParams = append(tParams, BBBUrlParam{
		name:  "attendeePW",
		value: Conf.AttendeePW,
	})
	tParams = append(tParams, BBBUrlParam{
		name:  "moderatorPW",
		value: Conf.ModeratorPW,
	})
	bbbQueryString := CreateQueryString("create", tParams)
	bbbCreateUrl := Conf.BbbServer + "create?" + bbbQueryString
	return bbbCreateUrl

}

func CreateBbbMeeting(pReq MeetingCreationRequest) (BBBCreateMeetingResponseBody, error) {
	var body BBBCreateMeetingResponseBody

	tr := &http.Transport{
		MaxIdleConns:       10,
		IdleConnTimeout:    30 * time.Second,
		DisableCompression: true,
	}

	tMeetingId, err := uuid.NewRandom()
	if err != nil {
		return body, err
	}

	tCreateUrl := CreateCreateUrl(tMeetingId.String())

	client := &http.Client{Transport: tr}
	resp, err := client.Get(tCreateUrl)
	if (err) != nil {
		return body, err
	}
	defer resp.Body.Close()

	rawBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return BBBCreateMeetingResponseBody{}, err
	}

	if err := xml.Unmarshal(rawBody, &body); err != nil {
		return BBBCreateMeetingResponseBody{}, err
	}

	if body.Returncode != "SUCCESS" {
		return BBBCreateMeetingResponseBody{}, errors.New("Request encountered an error: " + body.Returncode)
	}

	return body, nil
}

type BBBCreateMeetingResponseBody struct {
	XMLName              xml.Name `xml:"response"`
	Returncode           string   `xml:"returncode"`
	MeetingID            string   `xml:"meetingID"`
	InternalMeetingID    string   `xml:"internalMeetingID"`
	ParentMeetingID      string   `xml:"parentMeetingID"`
	AttendeePW           string   `xml:"attendeePW"`
	ModeratorPW          string   `xml:"moderatorPW"`
	CreateTime           string   `xml:"createTime"`
	VoiceBridge          string   `xml:"voiceBridge"`
	DialNumber           string   `xml:"dialNumber"`
	CreateDate           string   `xml:"createDate"`
	HasUserJoined        string   `xml:"hasUserJoined"`
	Duration             string   `xml:"duration"`
	HasBeenForciblyEnded string   `xml:"hasBeenForciblyEnded"`
	MessageKey           string   `xml:"messageKey"`
	Message              string   `xml:"message"`
}

type BBBMeetingLinks struct {
	ModeratorLink BBBJoinLink
	UserLinks     []BBBJoinLink
}

type BBBJoinLink struct {
	Name string
	Url  string
}

type BBBUrlParam struct {
	name  string
	value string
}
