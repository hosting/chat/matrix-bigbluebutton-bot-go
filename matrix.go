package main

import (
	"context"
	"errors"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
	"github.com/rs/zerolog/log"
	"maunium.net/go/mautrix"
	"maunium.net/go/mautrix/crypto/cryptohelper"
	"maunium.net/go/mautrix/event"
	"maunium.net/go/mautrix/format"
	"maunium.net/go/mautrix/id"
	"os"
	"regexp"
	"sync"
	"time"
)

func matrix() {
	client, err := mautrix.NewClient(Conf.MatrixHomeserver, "", "")
	if err != nil {
		panic(err)
	}

	var lastRoomID id.RoomID

	// create syncer
	syncer := client.Syncer.(*mautrix.DefaultSyncer)

	syncer.OnEventType(event.EventMessage, func(ctx context.Context, evt *event.Event) {
		lastRoomID = evt.RoomID

		if time.Now().UnixMilli()-evt.Timestamp > 30000 {
			//fmt.Println("msg too old, skipping ")
			return
		}

		botCommand := "!conf"
		match, _ := regexp.MatchString("^"+botCommand, evt.Content.AsMessage().Body)
		if !match {
			//fmt.Println("msg doesn't begin with bot command, skipping ")
			return
		}

		// don't react to own messages
		if evt.Sender.String() == client.UserID.String() {
			return

		}

		allMembers, err := client.JoinedMembers(ctx, lastRoomID)
		if err != nil {
			Close(err, 1)
		}

		members := []string{}

		for _, key := range allMembers.Joined {
			if key.DisplayName == "" || key.DisplayName == Conf.MatrixUser {
				continue
			}
			members = append(members, key.DisplayName)

		}

		meetingReq := MeetingCreationRequest{
			members,
			evt.Sender.String(),
		}
		meetingRequestChannel <- meetingReq

		go func() {
			for {
				val := <-chatResponseChannel
				content := format.HTMLToContent(val)
				_, err := client.SendMessageEvent(ctx, lastRoomID, event.EventMessage, &content)
				if err != nil {
					return
				}

			}
		}()
	})
	// join room if invited
	syncer.OnEventType(event.StateMember, func(ctx context.Context, evt *event.Event) {
		if evt.GetStateKey() == client.UserID.String() && evt.Content.AsMember().Membership == event.MembershipInvite {
			_, err := client.JoinRoomByID(ctx, evt.RoomID)
			if err == nil {
				lastRoomID = evt.RoomID
				//rl.SetPrompt(fmt.Sprintf("%s> ", lastRoomID))
				log.Info().
					Str("room_id", evt.RoomID.String()).
					Str("inviter", evt.Sender.String()).
					Msg("Joined room after invite")
			} else {
				log.Error().Err(err).
					Str("room_id", evt.RoomID.String()).
					Str("inviter", evt.Sender.String()).
					Msg("Failed to join room after invite")
			}
		}
	})

	cryptoHelper, err := cryptohelper.NewCryptoHelper(client, []byte(Conf.PickleKey), Conf.Database)
	if err != nil {
		panic(err)
	}

	cryptoHelper.LoginAs = &mautrix.ReqLogin{
		Type:       mautrix.AuthTypePassword,
		Identifier: mautrix.UserIdentifier{Type: mautrix.IdentifierTypeUser, User: Conf.MatrixUser},
		Password:   Conf.MatrixPassword,
	}

	err = cryptoHelper.Init(context.TODO())
	if err != nil {
		panic(err)
	}
	client.Crypto = cryptoHelper

	log.Info().Msg("Bot started!")
	syncCtx, cancelSync := context.WithCancel(context.Background())
	var syncStopWait sync.WaitGroup
	syncStopWait.Add(1)

	go func() {
		err = client.SyncWithContext(syncCtx)
		defer syncStopWait.Done()
		if err != nil && !errors.Is(err, context.Canceled) {
			panic(err)
		}
	}()

	for {
		select {
		case exitCode := <-terminateChannel:
			fmt.Println("received exitCode: ")
			fmt.Println(exitCode)
			cancelSync()
			syncStopWait.Wait()
			err = cryptoHelper.Close()
			os.Exit(exitCode)
		}
	}
}
